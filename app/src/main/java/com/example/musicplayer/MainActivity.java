package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Tells Main Activity has selected song at position
    void onUserSelectedSongAtPosition(int position) {
        switchSong(currentSongIndex, position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateDataModel();

        connectXMLViews();
        setupRecyclerView();
        displayCurrentSong();
        setupButtonHandlers();
    }

    void populateDataModel() {
        // Initialize properties of playlist
        playlist.name = "My Playlist";
        playlist.songs = new ArrayList<Song>();

        // Create and initialize the first song
        Song song = new Song();
        song.songName = "Beyond the Line";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.beyondtheline;
        song.mp3resource = R.raw.beyondtheline;

        // Adding the first song to the array of songs in the playlist
        playlist.songs.add(song);

        song = new Song();
        song.songName = "New Dawn";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.newdawn;
        song.mp3resource = R.raw.newdawn;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "of Elias Dream";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.ofeliasdream;
        song.mp3resource = R.raw.ofeliasdream;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Sad Day";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.sadday;
        song.mp3resource = R.raw.sadday;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Tomorrow";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.tomorrow;
        song.mp3resource = R.raw.tomorrow;

        playlist.songs.add(song);
    }

    void connectXMLViews() {
        songsRecyclerView = findViewById(R.id.song_list_view);
        imageView = findViewById(R.id.imageView2);
        songNameTextView = findViewById(R.id.musicTitle);
        artistNameTextView = findViewById(R.id.artistTitle);
        rewindButton = findViewById(R.id.rewind);
        playButton = findViewById(R.id.pause);
        nextButton = findViewById(R.id.ff);
        pauseButton = findViewById(R.id.play);

    }

    void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);

        // Connect the adapter to the recyclerView
        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsRecyclerView.setAdapter(songAdapter);
    }

    void displayCurrentSong() {
        Song currentSong = playlist.songs.get(currentSongIndex);
        imageView.setImageResource(currentSong.imageResource);
        songNameTextView.setText(currentSong.songName);
        artistNameTextView.setText(currentSong.artistName);
    }

    void setupButtonHandlers() {
        rewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //this will be called when the button is tapped
                System.out.println("Rewind button tapped.");
                if (currentSongIndex - 1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Next button tapped.");
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("play button tapped.");
                playCurrentSong();
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("pause Button tapped");
                pauseCurrentSong();

            }
        });


    }

    void switchSong(int fromIndex, int toIndex) {
        // Tell song adapter to refresh currently selected song
        songAdapter.notifyItemChanged(currentSongIndex);

        // Update current song index
        currentSongIndex = toIndex;

        // Display song
        displayCurrentSong();

        // Tell song adapter that song item at position to Index has changed
        songAdapter.notifyItemChanged(currentSongIndex);

        // Scroll to make current song visible in recyclerview
        songsRecyclerView.scrollToPosition(currentSongIndex);

        //Check if a current song is playing
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            // A song is playing, pause current song
            pauseCurrentSong();

            // Invalidate the media player
            mediaPlayer = null;

            // Play the new current song
            playCurrentSong();
        }
        else {
            // A song is not currently playing, invalidate the media player
            mediaPlayer = null;

        }
    }
    void pauseCurrentSong() {
        System.out.println("Pausing song at index" + currentSongIndex);
        // Check if media player already exists
        if (mediaPlayer != null) {
            // If media exists (id playing music), go ahead and pause it
            mediaPlayer.pause();

        }
    }

    void playCurrentSong() {
        System.out.println("Playing song at index" + currentSongIndex);
        // Check if mediaPlayer already exists
        if (mediaPlayer == null) {
            // mediaPlayer has not been created

            // Get the song object corresponding to the current song
            Song currentSong = playlist.songs.get(currentSongIndex);

            // Create a media player for the MP3 resource of the current song
            mediaPlayer = mediaPlayer.create(MainActivity.this, currentSong.mp3resource);
        }
        // Play the song
        mediaPlayer.start();
    }
    //Properties
    Playlist playlist = new Playlist();
    SongAdapter songAdapter;
    Integer currentSongIndex = 3;

    //MediaPlayer to play MP3
    MediaPlayer mediaPlayer = null;

    // XML views
    RecyclerView songsRecyclerView;
    ImageView imageView;
    TextView songNameTextView;
    TextView artistNameTextView;
    ImageButton rewindButton;
    ImageButton playButton;
    ImageButton nextButton;
    ImageButton pauseButton;

}